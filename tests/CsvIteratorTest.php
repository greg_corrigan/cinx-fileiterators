<?php

namespace CINX\File\Iterators\Tests;

require_once __DIR__ . '/../vendor/autoload.php';

use CINX\File\Iterators\CsvIterator;

class CsvIteratorTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @expectedException CINX\File\Iterators\FileIteratorException
     */
    public function testHeadersBeforeInit()
    {
        $fn = __DIR__ . '/sample.csv';
        $iterator = new CsvIterator($fn, true);
        $iterator->getHeader();
    }

    /**
     * @expectedException CINX\File\Iterators\FileIteratorException
     */
    public function testInvalidFile()
    {
        $fn = __DIR__ . '/asfasfsdfsample.csv';
        $iterator = new CsvIterator($fn, true);
        $iterator->getHeader();
    }

    public function testHeadersAfterInit()
    {
        $fn = __DIR__ . '/sample.csv';
        $iterator = new CsvIterator($fn, true);
        $iterator->iterate(function ($data, $obj) {
            $this->assertEquals(5, count($obj->getHeader()));
            $this->assertEquals('b', $obj->getHeader()[1]);
            return false;
        });
    }

    public function testGetValues()
    {
        $fn = __DIR__ . '/sample.csv';
        $iterator = new CsvIterator($fn, true);
        $rows = 0;
        $iterator->iterate(function ($data, $obj) use (&$rows) {
            $rows++;
            if ($rows === 1) {
                $this->assertEquals('4', $obj->{'d'});
                $this->assertEquals('4', $data['d']);
            }
            if ($rows === 2) {
                $this->assertEquals('6', $obj->{'b'});
            }
            if ($rows === 3) {
                $this->assertEquals('cat', $obj->c);
            }
            if ($rows === 4) {
                $this->assertEquals('pencil', $obj->unspaceme);
            }

        });
    }
}
