<?php

namespace CINX\File\Iterators;

interface IFileIterator
{
    public function iterate(callable $callback = null);
    public function getData();
    public function getHeader();
}
