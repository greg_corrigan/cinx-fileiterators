<?php

namespace CINX\File\Iterators;

use CINX\File\Iterators\FileIteratorException;

class CsvIterator implements IFileIterator
{
    protected $file;
    protected $headers = [];
    protected $has_headers = false;
    protected $data = [];
    protected $curr = null;
    private $init = false;

    public function __construct($file = null, $has_headers = false)
    {
        try {
            $this->file = new \SplFileObject($file);
        } catch (\Exception $ex) {
            throw new FileIteratorException(
                sprintf('Unable to open requested file [%s] for CsvIterator operation', $file)
            );
        }

        if (!$this->file->isReadable()) {
            throw new FileIteratorException(
                sprintf('Unable to read requested file [%s] for CsvIterator operation', $file)
            );
        }

        $this->file->setFlags(\SplFileObject::DROP_NEW_LINE
                            | \SplFileObject::READ_AHEAD
                            | \SplFileObject::SKIP_EMPTY
                            | \SplFileObject::READ_CSV);
        $this->has_headers = $has_headers;
    }

    public function getData()
    {
        if (!$this->init) {
            throw new FileIteratorException(
                sprintf('%s: Call to getData() prior to iterate method being called; rejected', __METHOD__)
            );
        }
        return $this->data;
    }

    public function getHeader()
    {
        if (!$this->init) {
            throw new FileIteratorException(
                sprintf('%s: Call to getHeader() prior to iterate method being called; rejected', __METHOD__)
            );
        }
        return $this->headers;
    }

    private function setHeader($data)
    {
        foreach ($data as $d) {
            $this->headers[] = Utils::cp1252ToUTF8(str_replace(" ", "", $d));
        }
    }

    public function iterate(callable $callback = null)
    {
        $this->init = true;
        $headerCheck = $this->has_headers;
        foreach ($this->file as $line => $data) {
            if ($headerCheck) {
                $this->setHeader($data);
                $headerCheck = false;
                continue;
            }
            while (count($data)<count($this->headers)) {
                $data[] = null;
            }

            foreach ($data as &$d) {
                $d = Utils::cp1252ToUTF8($d);
            }

            $data = array_combine($this->headers, $data);
            $this->data[$line] = $data;
            $this->curr = $data;
            if ($callback && $callback($data, $this) === false) {
                break;
            }
        }
    }

    public function __get($name)
    {
        if (!$this->init) {
            throw new FileIteratorException(
                sprintf('%s: Call to get value prior to iterate method being called; rejected', __METHOD__)
            );
        }
        return $this->curr[$name];
    }
}
